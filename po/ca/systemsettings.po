# Translation of systemsettings.po to Catalan
# Copyright (C) 2007-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2007, 2008, 2009, 2010, 2012, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2015, 2017, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: systemsettings\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-31 00:39+0000\n"
"PO-Revision-Date: 2023-06-14 23:00+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 22.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Josep M. Ferrer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "txemaq@gmail.com"

#: app/main.cpp:54 app/SettingsBase.cpp:51
#, kde-format
msgid "Info Center"
msgstr "Centre d'informació"

#: app/main.cpp:56
#, kde-format
msgid "Centralized and convenient overview of system information."
msgstr "Resum centralitzat i pràctic de la informació del sistema."

#: app/main.cpp:58 app/main.cpp:69
#, kde-format
msgid "(c) 2009, Ben Cooksley"
msgstr "(c) 2009, Ben Cooksley"

#: app/main.cpp:65 app/SettingsBase.cpp:54 app/sidebar/qml/IntroPage.qml:51
#: runner/systemsettingsrunner.cpp:108
#, kde-format
msgid "System Settings"
msgstr "Arranjament del sistema"

#: app/main.cpp:67
#, kde-format
msgid "Central configuration center by KDE."
msgstr "Centre per a la configuració central, creat per la comunitat KDE."

#: app/main.cpp:80
#, kde-format
msgid "Ben Cooksley"
msgstr "Ben Cooksley"

#: app/main.cpp:80
#, kde-format
msgid "Maintainer"
msgstr "Mantenidor"

#: app/main.cpp:81
#, kde-format
msgid "Marco Martin"
msgstr "Marco Martin"

#: app/main.cpp:81
#, kde-format
msgid "Author"
msgstr "Autor"

#: app/main.cpp:82
#, kde-format
msgid "Mathias Soeken"
msgstr "Mathias Soeken"

#: app/main.cpp:82
#, kde-format
msgid "Developer"
msgstr "Desenvolupador"

#: app/main.cpp:83
#, kde-format
msgid "Will Stephenson"
msgstr "Will Stephenson"

#: app/main.cpp:83
#, kde-format
msgid "Internal module representation, internal module model"
msgstr "Representació interna dels mòduls, model intern dels mòduls"

#: app/main.cpp:91
#, kde-format
msgid "List all possible modules"
msgstr "Llista tots els mòduls possibles"

#: app/main.cpp:92 app/main.cpp:153
#, kde-format
msgid "Configuration module to open"
msgstr "Mòdul de configuració a obrir"

#: app/main.cpp:93 app/main.cpp:154
#, kde-format
msgid "Arguments for the module"
msgstr "Arguments per al mòdul"

#: app/main.cpp:101
#, kde-format
msgid "The following modules are available:"
msgstr "Estan disponibles els mòduls següents:"

#: app/main.cpp:119
#, kde-format
msgid "No description available"
msgstr "No hi ha disponible cap descripció"

#: app/SettingsBase.cpp:136
#, kde-format
msgid "Highlight Changed Settings"
msgstr "Ressaltat dels paràmetres canviats"

#: app/SettingsBase.cpp:147
#, kde-format
msgid "Report a Bug in the Current Page…"
msgstr "Informa d'un error a la pàgina actual…"

#: app/SettingsBase.cpp:169
#, kde-format
msgid "Help"
msgstr "Ajuda"

#: app/sidebar/qml/CategoriesPage.qml:62
#, kde-format
msgid "Show intro page"
msgstr "Mostra la pàgina d'introducció"

#: app/sidebar/qml/CategoriesPage.qml:130
#, kde-format
msgctxt "A search yielded no results"
msgid "No items matching your search"
msgstr "No hi ha cap element que coincideixi amb la cerca"

#: app/sidebar/qml/HamburgerMenuButton.qml:25
#, kde-format
msgid "Show menu"
msgstr "Mostra el menú"

#: app/sidebar/qml/IntroPage.qml:44
#, kde-format
msgid "Plasma"
msgstr "Plasma"

#: app/sidebar/SidebarMode.cpp:642
#, kde-format
msgid "Sidebar"
msgstr "Barra lateral"

#: app/sidebar/SidebarMode.cpp:712
#, kde-format
msgid "Most Used"
msgstr "Més usat"

#. i18n: ectx: ToolBar (mainToolBar)
#: app/systemsettingsui.rc:15
#, kde-format
msgid "About System Settings"
msgstr "Quant a l'Arranjament del sistema"

#: app/ToolTips/tooltipmanager.cpp:187
#, kde-format
msgid "Contains 1 item"
msgid_plural "Contains %1 items"
msgstr[0] "Conté 1 element"
msgstr[1] "Conté %1 elements"

#: core/ExternalAppModule.cpp:25
#, kde-format
msgid "%1 is an external application and has been automatically launched"
msgstr "%1 és una aplicació externa i s'engega automàticament"

#: core/ExternalAppModule.cpp:26
#, kde-format
msgid "Relaunch %1"
msgstr "Torna a engegar el %1"

#. i18n: ectx: property (windowTitle), widget (QWidget, ExternalModule)
#: core/externalModule.ui:14
#, kde-format
msgid "Dialog"
msgstr "Diàleg"

#: core/ModuleView.cpp:185
#, kde-format
msgid "Reset all current changes to previous values"
msgstr "Restaura tots els canvis actuals als valors anteriors"

#: core/ModuleView.cpp:360
#, kde-format
msgid ""
"The settings of the current module have changed.\n"
"Do you want to apply the changes or discard them?"
msgstr ""
"L'arranjament del mòdul actual ha canviat.\n"
"Voleu aplicar els canvis o descartar-los?"

#: core/ModuleView.cpp:365
#, kde-format
msgid "Apply Settings"
msgstr "Aplica l'arranjament"

#: runner/systemsettingsrunner.cpp:32
#, kde-format
msgid "Finds system settings modules whose names or descriptions match :q:"
msgstr ""
"Cerca mòduls de l'arranjament del sistema en què els noms o les descripcions "
"coincideixen amb :q:"

#: runner/systemsettingsrunner.cpp:106
#, kde-format
msgid "System Information"
msgstr "Informació del sistema"
